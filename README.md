# M4_Lab1_DB


## Getting started
The code for sending query using both statements and prepared statements is present in ```src/main/QueryExecutor.java```.

Each exercise has a corresponding class with main method in it, the results are always printed in console.

For program to work please enter our DB's credentials(provided in PDF) into the file ```ServerCredentials.java```.
### Exercise 11 (performance test):

In all three test cases the results were of the same dynamics: ```Statement``` > ```Prepared Statement``` > ```Stored Procedures```.
Meaning the ```Statement``` performed the worst among all of them, and the ```Stored Procedure``` presented the best score.  