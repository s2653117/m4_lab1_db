package main.exercise6;

import main.ExerciseRunner;
import main.QueryExecutor;

public class Exercise6Runner implements ExerciseRunner {
    private static final String query =
            "SELECT DISTINCT p.name\n" +
            "FROM person p, writes w, (SELECT m.mid\n" +
            "          FROM movie m, acts a, person p, writes w\n" +
            "          WHERE w.mid=m.mid AND m.mid=a.mid AND a.pid=p.pid AND p.name = ?\n" +
            "          GROUP BY m.mid) h\n" +
            "WHERE p.pid=w.pid AND w.mid=h.mid";

    @Override
    public void execute(boolean printResults) {
        QueryExecutor queryExecutor = new QueryExecutor();
        queryExecutor.start();

        queryExecutor.sendPreparedStatement(query, "Harrison Ford");

        if (printResults) {
            queryExecutor.printLastQueryResult();
            System.out.print("\n\nSecond query(Bruce Willis): \n\n");
        }

        queryExecutor.sendPreparedStatement(query, "Bruce Willis");

        if (printResults) {
            queryExecutor.printLastQueryResult();
        }

        queryExecutor.close();
    }


    public static void main(String[] args) {
        (new Exercise6Runner()).execute(true);
    }
}
