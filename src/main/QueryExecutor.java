package main;

import java.sql.*;

public class QueryExecutor {
    private final String name;
    private final String password;
    private final String port;
    private final String host;
    private final String schema;

    private Connection connection;
    private ResultSet result;
    private ResultSetMetaData resultData;

    public QueryExecutor() {
        name = ServerCredentials.NAME;
        password = ServerCredentials.PASSWORD;
        port = ServerCredentials.PORT;
        host = ServerCredentials.HOST;
        schema = ServerCredentials.SCHEMA;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException cnfe) {
            System.err.println("Error loading the driver: " + cnfe);
        }
    }

    public void start() {
        String url = "jdbc:postgresql://" + host + ":" + port + "/" + name + "?currentSchema=" + schema;

        try {
            connection = DriverManager.getConnection(url, name, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void sendStatement(String query) {
        try {
            Statement statement = connection.createStatement();

            result = statement.executeQuery(query);
            resultData = result.getMetaData();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void sendPreparedStatement(String query, String... args) {
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < args.length; i++) {
                if (args[i].matches("[0-9]+")) {
                    statement.setInt(i + 1, Integer.parseInt(args[i]));
                } else {
                    statement.setString(i + 1, args[i]);
                }
            }

            result = statement.executeQuery();
            resultData = result.getMetaData();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void printLastQueryResult() {
        if (result == null) return;

        int counter = 0;
        try {
            while (result.next()) {
                counter++;
                for (int i = 1; i <= resultData.getColumnCount(); i++) {
                    System.out.print(result.getString(i));
                    if (i < resultData.getColumnCount()) {
                        System.out.print(" || ");
                    }
                }
                System.out.print("\n");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.printf("Number of rows: " + counter);

        result = null;
        resultData = null;
    }

    public ResultSet getResult() {
        return result;
    }

    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
