package main.exercise12;

import main.ExerciseRunner;
import main.QueryExecutor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Exercise12Runner implements ExerciseRunner {
    private static final String queryForAllMovies =
            "SELECT mid\n" +
            "FROM movie";

    private static final String queryForAuthorsOfMovieX =
            "SELECT p.name\n" +
            "FROM writes w, person p\n" +
            "WHERE w.mid = ? \n" +
            "AND w.pid = p.pid";

    private static final String queryForAllMoviesWithDirector =
            "SELECT DISTINCT m.mid\n" +
            "FROM movie m, directs d\n" +
            "WHERE m.mid=d.mid";

    private final QueryExecutor queryExecutor;

    public static void main(String[] args) {
        (new Exercise12Runner()).execute(true);
    }

    public Exercise12Runner() {
        queryExecutor = new QueryExecutor();
        queryExecutor.start();
    }

    @Override
    public void execute(boolean printResults) {
        List<String> authors = new ArrayList<>();

        for (Integer mid : allMovies(false)) {
            if (!hasDirector(mid)) {
                authors.addAll(authorOfMovie(mid));
            }
        }

        if (printResults) {
            System.out.print("Authors of the films without directors: \n");
            for (String author : authors) {
                System.out.println(author);
            }
        }

        queryExecutor.close();
    }

    // if withDirector set to true, the returning list will contain only movies, which have director(s)
    private List<Integer> allMovies(boolean withDirector) {
        List<Integer> resultList = new ArrayList<>();

        if (withDirector) {
            queryExecutor.sendStatement(queryForAllMoviesWithDirector);
        } else {
            queryExecutor.sendStatement(queryForAllMovies);
        }
        ResultSet result = queryExecutor.getResult();

        try {
            while (result.next()) {
                ResultSetMetaData resultData = result.getMetaData();
                for (int i = 1; i <= resultData.getColumnCount(); i++) {
                    resultList.add(Integer.parseInt(result.getString(i)));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return resultList;
    }

    private List<String> authorOfMovie(int mid) {
        List<String> resultList = new ArrayList<>();

        queryExecutor.sendPreparedStatement(queryForAuthorsOfMovieX, String.valueOf(mid));
        ResultSet result = queryExecutor.getResult();

        try {
            while (result.next()) {
                ResultSetMetaData resultData = result.getMetaData();
                for (int i = 1; i <= resultData.getColumnCount(); i++) {
                    resultList.add(result.getString(i));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return resultList;
    }

    private boolean hasDirector(int mid) {
        List<Integer> allMoviesWithDirector = allMovies(true);
        return allMoviesWithDirector.contains(mid);
    }
}
