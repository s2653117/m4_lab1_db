package main.exercise12;

public class PerformanceTest {
    public static void main(String[] args) {
        int iters = 5;
        long startTime = System.currentTimeMillis();
        // preparatory statements outside the loop,
        // e.g., Statement st = conn.createStatement();
        for (int i = 0; i < iters; i++) {
            (new Exercise12Runner()).execute(false);
        }
        // rounding up statements outside the loop.
        // e.g., st.close();
        long stopTime = System.currentTimeMillis();
        double elapsedTime = (stopTime - startTime) / (1.0 * iters);
        System.out.println("Measured time: " + elapsedTime + " ms");
    }
}
