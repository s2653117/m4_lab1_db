package main;

public interface ExerciseRunner {
    void execute(boolean printResults);
}
