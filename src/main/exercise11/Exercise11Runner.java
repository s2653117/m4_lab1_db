package main.exercise11;

import main.ExerciseRunner;
import main.exercise5.Exercise5Runner;
import main.exercise6.Exercise6Runner;
import main.exercise8.Exercise8Runner;

import java.util.HashMap;
import java.util.Map;

public class Exercise11Runner implements ExerciseRunner {
    public static void main(String[] args) {
        (new Exercise11Runner()).execute(true);
    }

    @Override
    public void execute(boolean printResults) {
        String[] testsMessages = new String[]{"Statement (ex. 5)", "Prepared Statement (ex. 6)", "Stored Procedure (ex. 8)"};
        Map<Integer, ExerciseRunner> tests = new HashMap<>();

        tests.put(0, new Exercise5Runner());
        tests.put(1, new Exercise6Runner());
        tests.put(2, new Exercise8Runner());

        for (int t = 0; t < 3; t++) {
            System.out.print("\u001B[33mTest: " + testsMessages[t] + "\n\n\u001B[0m");

            int iters = 100;
            long startTime = System.currentTimeMillis();
            // preparatory statements outside the loop,
            // e.g., Statement st = conn.createStatement();
            for (int i = 0; i < iters; i++) {
                tests.get(t).execute(false);
            }
            // rounding up statements outside the loop.
            // e.g., st.close();
            long stopTime = System.currentTimeMillis();
            double elapsedTime = (stopTime - startTime) / (1.0 * iters);
            System.out.println("Measured time: " + elapsedTime + " ms");
        }
    }
}
