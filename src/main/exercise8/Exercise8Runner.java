package main.exercise8;

import main.ExerciseRunner;
import main.QueryExecutor;
import main.exercise5.Exercise5Runner;

public class Exercise8Runner implements ExerciseRunner {
    private static final String query = "SELECT AuthorsOfMoviesOfActor(?);";

    @Override
    public void execute(boolean printResults) {
        QueryExecutor queryExecutor = new QueryExecutor();
        queryExecutor.start();

        queryExecutor.sendPreparedStatement(query, "Harrison Ford");

        if (printResults) {
            queryExecutor.printLastQueryResult();
            System.out.print("\n\nSecond query(Bruce Willis): \n\n");
        }

        queryExecutor.sendPreparedStatement(query, "Bruce Willis");

        if (printResults) {
            queryExecutor.printLastQueryResult();
        }

        queryExecutor.close();
    }

    public static void main(String[] args) {
        (new Exercise8Runner()).execute(true);
    }
}
