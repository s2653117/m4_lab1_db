package main.exercise5;

import main.ExerciseRunner;
import main.QueryExecutor;

public class Exercise5Runner implements ExerciseRunner {
    private static final String query =
            "SELECT DISTINCT p.name\n" +
            "FROM person p, writes w, (SELECT m.mid\n" +
            "          FROM movie m, acts a, person p, writes w\n" +
            "          WHERE w.mid=m.mid AND m.mid=a.mid AND a.pid=p.pid AND p.name = 'Harrison Ford'\n" +
            "          GROUP BY m.mid) h\n" +
            "WHERE p.pid=w.pid AND w.mid=h.mid";

    @Override
    public void execute(boolean printResults) {
        QueryExecutor queryExecutor = new QueryExecutor();
        queryExecutor.start();

        queryExecutor.sendStatement(query);

        if(printResults) {
            queryExecutor.printLastQueryResult();
        }

        queryExecutor.close();
    }

    public static void main(String[] args) {
        (new Exercise5Runner()).execute(true);
    }

}
